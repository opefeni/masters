from skimage import feature
from pylab import *
from PIL import Image,ImageEnhance


class LocalBinaryPatterns:

    def __init__(self, numPoint, radius):
        self.numPoint = numPoint
        self.radius = radius


    def extract_descriptor(self, image, eps=1e7):
        lbp = feature.local_binary_pattern(image, self.numPoint, self.radius, method='uniform')
        hist, _ = histogram(lbp.ravel(),bins=arange(0, self.numPoint + 3), range=arange(0, self.radius + 2))
        # normalise the histogram
        hist = hist.astype('float')
        hist /= (hist.sum() + eps)

        return hist


class VisualAppearance(object):

    def extract_contrast(self, image):
        im = Image.open(image).convert('L')
        # obtained max and min intensity
        max = im.max() * 1.0
        min = im.min() * 1.0

        # compute the image contrast factor
        factor = (max - min) / (max + min)

        enhancer = ImageEnhance.Contrast(im)
        enhancer.enhance(factor)

        hist, _ = histogram(im.flatten(), bins=128,  density=True)

        return hist
