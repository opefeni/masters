from PIL import Image, ImageEnhance
from pylab import *
from skimage import color, feature
from scipy.ndimage import filters
import sift, colorsys


def extract_hsv_features(im):
    im = color.rgb2hsv(array(Image.open(im)))
    h, s, v = im[:,:,0], im[:,:,1], im[:,:,2]
    H_hist, h1 = histogram(h.flatten(), bins=128, density=True)
    S_hist, h2 = histogram(s.flatten(), bins=128, density=True)
    V_hist, h3 = histogram(v.flatten(), bins=128, density=True)

    return H_hist, S_hist, V_hist


def extract_gradient_features(im):
    features = zeros([len(im), 128])
    im = array(Image.open(im).convert("L"))
    # print im.shape
    # Sobel derivative filters
    imx = zeros(im.shape)
    filters.sobel(im, 1, imx)

    imy = zeros(im.shape)
    filters.sobel(im, 0, imy)

    magnitude = sqrt(imx ** 2 + imy ** 2)
    hist, _ = histogram(magnitude.flatten(), bins=128, density=True)

    return hist

def extract_sift_features(im):

    # sift.process_image(im, im[:-3]+'sift')

    loc1, descr = sift.read_features_from_file(im[:-3]+'sift')
    # print descr.shape
    hist, _ = histogram(descr.flatten(), bins=128,  density=True)

    return hist


def extract_contrast_features(img):
    im = Image.open(img)
    img = array(im)
    # obtained max and min intensity
    max = img.max() * 1.0
    min = img.min() * 1.0

    # compute the image contrast factor
    factor = (max - min) / (max + min)

    enhancer = ImageEnhance.Contrast(im)
    im2 = enhancer.enhance(factor)

    hist, _ = histogram(im2, bins=128,  density=True)

    return hist


def extract_lbp_features(im, numPoint=24, radius=8, eps=1e7):
    """ Extraction of local binary pattern with radius 8 x 8 boxes division"""
    image = array(Image.open(im).convert("L"))
    lbp = feature.local_binary_pattern(image, numPoint, radius, method='uniform')
    hist, _ = histogram(lbp.ravel(), bins=128, range=(0, numPoint + 2))


    # normalise the histogram
    hist = hist.astype('float')
    hist /= (hist.sum() + eps)

    return hist
