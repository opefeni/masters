from sklearn.svm import SVC
from sklearn.ensemble import RandomForestClassifier, ExtraTreesClassifier, AdaBoostClassifier, GradientBoostingClassifier
from sklearn.model_selection import cross_val_score
from sklearn.metrics import accuracy_score
import  os
from pylab import *
import wfeatures as ft

test_path = "dataset1/test"
training_path = "dataset1/training"

imlist =[os.path.join(training_path, f) for f in os.listdir(training_path) if f.endswith('.jpg')]
labels = [im.split('/')[-1][:2] for im in imlist]

# create conversion function for the labels
def convert_labels(labels, transl):
    """ Convert between strings and numbers. """
    return [transl[l] for l in labels]
transl = {}

for i, c in enumerate(labels):
    transl[c], transl[i] = i, c

labels = convert_labels(labels, transl)

print "Processing HSV features....."
h, s, v, _, _, _ = ft.extract_hsv_features(imlist)

print "Processing gradient features....."
grad, _ = ft.extract_gradient_features(imlist)
#
print "Processing SIFT features....."
sf, _ = ft.extract_sift_features(imlist)
#
print "Processing Contrast features....."
contrast, _= ft.extract_contrast_features(imlist)
#
print "Processing LBP features....."
lbp, _ = ft.extract_lbp_features(imlist)


# Initialise ensemble methods

clf_hu = RandomForestClassifier(n_estimators=500)
clf_sa = ExtraTreesClassifier(n_estimators=500)
clf_va = AdaBoostClassifier(n_estimators=500)
clf_gr = GradientBoostingClassifier(n_estimators=500)
clf_lb = ExtraTreesClassifier(n_estimators=500)
clf_sf = AdaBoostClassifier(n_estimators=500)
clf_con = RandomForestClassifier(n_estimators=500)


base_algorithms = [clf_hu, clf_sa, clf_va, clf_gr, clf_lb, clf_sf, clf_con]
base_features = [h, s, v, grad, lbp, sf, contrast]
base_labels = ['Hue', 'Saturation', 'Value', 'Gradient', 'LBP', 'SIFT', 'Contrast']

stacked_features = zeros((len(labels), len(base_algorithms)))
stacked_important_features = zeros((len(labels), len(base_algorithms)))

clf_hu.fit(h[:40], labels[:40])
clf_sa.fit(s[:40], labels[:40])
clf_va.fit(v[:40], labels[:40])
clf_gr.fit(grad[:40], labels[:40])
clf_lb.fit(lbp[:40], labels[:40])
clf_sf.fit(sf[:40], labels[:40])
clf_con.fit(contrast[:40], labels[:40])
hue_importance_features = clf_hu.feature_importances_

for clf, X, label in zip(base_algorithms, base_features, base_labels):
    score = cross_val_score(clf, X=X, y=labels, cv=5, scoring='accuracy')
    clf.fit(X[:40], labels[:40])
    label_important_features = clf.feature_importances_
    # score = clf.score(X[40:], labels[40:])
    print "Accuracy: %0.2f (+/- %0.2f) [%s]" % (score.mean(), score.std(), label)


important_features = concatenate((Hue_important_features, Saturation_important_features, Value_important_features,
                                  Gradient_important_features, LBP_important_features, SIFT_important_features,
                                  Contrast_important_features), axis=1)


print len(important_features)
print type(important_features)
# print important_features[0]
# print "Accuracy: %0.2f (+/- %0.2f) " % (score.mean(), score.std())
# print "Class Weight Coefficient: ", clf.coef0
# print "Class Weight: ", clf.class_weight