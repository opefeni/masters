from pylab import *


class KnnClassifier(object):

    def __init__(self, label, samples):
        self.label = label
        self.sample = samples

    def classify(self, point, k=3,):
        """class a point against training data and return label"""

        # compute distance to all training points
        distance = array([ self.pointDistance(point, s) for s in self.sample])

        # sort()
        ndist = distance.argsort()

        # initialise empty dictionary
        votes = {}

        for i in range(k):
            label = self.label[ndist[i]]
            votes.setdefault(label, 0)
            votes[label] += 1

        return max(votes)

    def pointDistance(self,d1,d2):
        """ using euclidean to computer the distance between two points"""
        return sqrt(sum(d1 - d2) ** 2)