from sklearn.cross_validation import StratifiedKFold
from sklearn.svm import SVC
from sklearn.naive_bayes import GaussianNB
from sklearn.neighbors import KNeighborsClassifier
from sklearn import metrics
from sklearn.model_selection import cross_val_score, train_test_split
from sklearn.linear_model import LogisticRegression
from sklearn.preprocessing import LabelEncoder
import xgboost as xgb
from sklearn.ensemble import RandomForestClassifier, ExtraTreesClassifier, GradientBoostingClassifier, AdaBoostClassifier
import matplotlib.pyplot as plt
import numpy as np
import pickle
import itertools



def stack_classifier(data, label):

    X, y = data, label

    # The DEV SET will be used for all training and validation purposes
    # The TEST SET will never be used for training, it is the unseen test
    dev_cut = len(y) * 4/5
    X_dev = X[:dev_cut]
    Y_dev = y[:dev_cut]
    X_test = X[dev_cut:]
    Y_test = y[dev_cut:]

    n_trees = 100
    n_folds = 5

    # Ready for cross validation
    skf = list(StratifiedKFold(Y_dev, n_folds))

    # Our level 0 classifier
    clfs = [
        RandomForestClassifier(n_estimators=n_trees, criterion='gini', n_jobs=-1),
        # ExtraTreesClassifier(n_estimators=n_trees, criterion='gini', n_jobs=-1),
        KNeighborsClassifier(n_neighbors=5),
        SVC(kernel='rbf'),
        # GaussianNB(),
        # GradientBoostingClassifier(n_estimators=n_trees)
    ]

    # Pre-allocate the data
    stack_train = np.zeros((X_dev.shape[0], len(clfs)))
    stack_test = np.zeros((X_test.shape[0], len(clfs)))


    # For each classifier, we train the number of  fold times (=len(skf))

    for j, clf in enumerate(clfs):
        print "Training classifier [%s]" %(j)
        stack_test_j = np.zeros((X_test.shape[0], len(skf)))
        for i, (train_index, cv_index) in enumerate(skf):
            print "Fold [%s]" %(i)

            # This is the training and validation set
            X_train = X_dev[train_index]
            Y_train = Y_dev[train_index]

            X_cv = X_dev[cv_index]
            Y_cv = Y_dev[cv_index]

            clf.fit(X_train, Y_train)

            # This output will be the basis for our blended classifier to train against
            # which is also the output of our classifier
            stack_train[cv_index, j] = clf.predict(X_cv)
            stack_test_j[:, i] = clf.predict(X_test)

        # Get the mean predictions of the cross validation sets
        stack_test[:, j] = stack_test_j.mean(1)

    print 'Y_dev.shape = %s' %(Y_dev.shape)

    return stack_train, stack_test, Y_dev, Y_test


def plot_confusion_matrix(cm, classes,
                          normalize=False,
                          title='Confusion matrix',
                          cmap=plt.cm.Blues):
    """
    This function prints and plots the confusion matrix.
    Normalization can be applied by setting `normalize=True`.
    """
    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
        print("Normalized confusion matrix")
    else:
        print('Confusion matrix, without normalization')

    print(cm)

    plt.imshow(cm, interpolation='nearest', cmap=cmap)
    plt.title(title)
    plt.colorbar()
    tick_marks = np.arange(len(classes))
    plt.xticks(tick_marks, classes, rotation=45)
    plt.yticks(tick_marks, classes)

    fmt = '.2f' if normalize else 'd'
    thresh = cm.max() / 2.
    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
        plt.text(j, i, format(cm[i, j], fmt),
                 horizontalalignment="center",
                 color="white" if cm[i, j] > thresh else "black")

    plt.tight_layout()
    plt.ylabel('Labels')
    plt.xlabel('Predictions')


def classifier(features, Y_dev, test, Y_test, weight=False,cnf_matrix=False, feature_name=None):
    if cnf_matrix is True:
        clf = GradientBoostingClassifier(n_estimators=100)
        # clf = SVC(kernel='rbf')

    else:
        clf = SVC(kernel='linear')


    # train the classifier
    clf.fit(features, Y_dev)
    #
    # xg_train = xgb.DMatrix(features, label=Y_dev)
    # xg_test = xgb.DMatrix(test, label=Y_test)
    #
    # # setup parameter for the xgboost
    # param = {}
    #
    # param['eta'] = 0.1
    # param['max_depth'] = 6
    # param['silent'] = 1
    # param['nthread'] = 4
    # param['num_class'] = 6
    #
    # watchlist = [(xg_train, 'train'), (xg_test, 'test')]
    # num_round = 5
    #
    # clf = xgb.train(param,xg_train,num_round, watchlist)

    # Predict now
    Y_predict = clf.predict(test)
    score = metrics.accuracy_score(Y_test, Y_predict)
    if weight is True:
        print "%s Weight = %s " % (feature_name, clf.coef_)
    print "%s Accurary = %s" % (feature_name, score)

    # Features Ranking

    if cnf_matrix is True:
        # Compute confusion matrix
        cnf_matrix = metrics.confusion_matrix(Y_test, Y_predict)
        np.set_printoptions(precision=2)

        # Plot normalized confusion matrix
        plt.figure()
        plot_confusion_matrix(cnf_matrix, classes=class_names, normalize=True,
                              title='Confusion Matrix')
        # plot_features(features)

    else:
        return score

def plot_features(scores):
    label = ('Hue', 'Saturation', 'Value', 'Gradient', 'LBP', 'Contrast')
    y_pos = np.arange(len(label))
    plt.figure()
    plt.title('Feature Accuracy')
    plt.bar(y_pos, scores, color='b', align='center', alpha=0.5)
    plt.xticks(y_pos, label)
    plt.ylabel(' Classification Accuracy')


def other_method(X, y):
    print "training lenght ", len(X)
    print "label length ", len(y)

    n_trees = 100
    clfs = [
        KNeighborsClassifier(n_neighbors=5),
        SVC(kernel='rbf'),
        GaussianNB(),
        RandomForestClassifier(n_estimators=n_trees, criterion='gini', n_jobs=-1),
        ExtraTreesClassifier(n_estimators=n_trees * 2, criterion='gini', n_jobs=-1),
        # AdaBoostClassifier(n_estimators=n_trees)
        GradientBoostingClassifier(n_estimators=n_trees)
    ]


    for clf, clf_label in zip(clfs,['KNN','SVM','Native Baye','Random Forest',' Extra Tree', 'Gradient Boost']):
        # clf.fit(X_train, y_train)
        # score = clf.score(X_test,y_test)
        score = cross_val_score(clf, X=X, y=y, cv=5, scoring='accuracy')
        print "%s Accuracy: %0.2f (%0.2f)" % (clf_label, score.mean(), score.std())



if __name__ == '__main__':

    # extract features from the saved pickle data
    with open('features2.pkl', 'rb') as f:
        hfeatures = pickle.load(f)
        sfeatures = pickle.load(f)
        vfeatures = pickle.load(f)
        gfeatures = pickle.load(f)
        # sifeatures = pickle.load(f)
        cfeatures = pickle.load(f)
        lfeatures = pickle.load(f)
        labels = pickle.load(f)

    # convert label string to numeric
    label_encoder = LabelEncoder()
    label_encoder.fit(labels)
    labels = label_encoder.transform(labels)

    original_features = np.concatenate((hfeatures,sfeatures,vfeatures,gfeatures,cfeatures,lfeatures), axis=1)

    class_names = label_encoder.classes_

    # Train each features
    hue_stack_train, hue_stack_test, Y_dev, Y_test = stack_classifier(hfeatures, labels)
    sat_stack_train, sat_stack_test, Y_dev, Y_test = stack_classifier(sfeatures, labels)
    val_stack_train, val_stack_test, Y_dev, Y_test = stack_classifier(vfeatures, labels)
    gra_stack_train, gra_stack_test, Y_dev, Y_test = stack_classifier(gfeatures, labels)
    # sif_stack_train, sif_stack_test, Y_dev, Y_test = stack_classifier(sifeatures, labels)
    con_stack_train, con_stack_test, Y_dev, Y_test = stack_classifier(cfeatures, labels)
    lbp_stack_train, lbp_stack_test, Y_dev, Y_test = stack_classifier(lfeatures, labels)

    # generate individual feature accuracy
    hue = classifier(hue_stack_train, Y_dev, hue_stack_test,  Y_test, feature_name='Hue')
    sat = classifier(sat_stack_train, Y_dev, sat_stack_test,  Y_test, feature_name='Saturation')
    va = classifier(val_stack_train, Y_dev, val_stack_test,  Y_test, feature_name='Value')
    gra = classifier(gra_stack_train, Y_dev, gra_stack_test,  Y_test, feature_name='Gradient')
    # classifier(sif_stack_train, Y_dev, sif_stack_test,  Y_test, feature_name='SIFT')
    lbp = classifier(lbp_stack_train, Y_dev, lbp_stack_test,  Y_test, feature_name='LBP')
    con = classifier(con_stack_train, Y_dev, con_stack_test,  Y_test, feature_name='Contrast')

    # concatenate the stacked trained features and test features
    features = np.concatenate((hue_stack_train, sat_stack_train, val_stack_train, gra_stack_train,  con_stack_train, lbp_stack_train), axis=1)

    test = np.concatenate((hue_stack_test, sat_stack_test, val_stack_test, gra_stack_test, con_stack_test, lbp_stack_test), axis=1)


    # start blending
    classifier(features, Y_dev, test, Y_test, cnf_matrix=True, feature_name="Stacking")

    other_method(original_features, labels)
    
    
    plot_features([hue, sat, va, gra, lbp, con])

    plt.show()
    # # Other methods
    # # print len(labels)
    # print len(features)
    


