from scipy.cluster.vq import *
import sift
from pylab import *


class Vocabulary(object):

    def __init__(self, name):
        self.name = name
        self.voc = []
        self.idf = []
        self.trainingdata = []
        self.num_words = []


    def train(self, featurefiles, k=100, subsampling=10):
        """Training features from feature list using k mean of 100
            and subsampling of 10
        """

        # get the featureList number of rows
        num_images = len(featurefiles)

        # initialised empty descriptor list
        descr = []

        #append the first index of the featuresfile for easy stacking
        descr.append(featurefiles[0]) # [1] added to featurefiles indicate the descriptor path
        descriptors = descr[0]

        #stack others to the first index
        for i in arange(1, num_images):
            descr.append(featurefiles[i])
            descriptors = vstack((descriptors, descr[i]))

        #compute the K-mean with number of iteration equals 1 as against the default of 20
        self.voc, distortion = kmeans(descriptors[::subsampling, :], k, 1)
        self.num_words = self.voc.shape[0]

        print "Number of words : ", self.num_words
        print "Number of image : ", num_images
        print "descr ", descriptors.shape
        print "**********************************"
        print self.voc.shape

        #go through all the images and project on the vocabulary
        imwords = zeros((num_images, self.num_words))
        for i in range(num_images):
            imwords[i] = self.project(descr[i])

        nbr_occurences = sum((imwords > 0) * 1, axis=0)

        self.idf = log((1.0 * num_images) / (1.0 * nbr_occurences + 1))

        self.trainingdata = featurefiles
        self.feature_vectors = imwords

    def project(self, descriptor):
        """ Project the image descriptor on the vocabulary
            inorder to create word histogram
        """
        imhist = zeros((self.num_words))
        words, distance = vq(descriptor, self.voc)
        for w in words:
            imhist[w] += 1

        return imhist
