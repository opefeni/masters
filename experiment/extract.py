import os, pickle
from pylab import *
import wfeatures as ft

path = "dataset2"

imlist =[os.path.join(path, f) for f in os.listdir(path) if f.endswith('.jpg')]
labels = [im.split('/')[-1][:2] for im in imlist]


print "Processing HSV features....."
h, s, v, _, _, _ = ft.extract_hsv_features(imlist)

print "Processing gradient features....."
grad, _ = ft.extract_gradient_features(imlist)
#
print "Processing SIFT features....."
sf, _ = ft.extract_sift_features(imlist)

print "Processing Contrast features....."
contrast, _= ft.extract_contrast_features(imlist)
#
print "Processing LBP features....."
lbp, _ = ft.extract_lbp_features(imlist)


with open('features.pkl','wb') as f:
    pickle.dump(h, f)
    pickle.dump(s, f)
    pickle.dump(v, f)
    pickle.dump(grad, f)
    pickle.dump(sf, f)
    pickle.dump(contrast, f)
    pickle.dump(lbp, f)
    pickle.dump(labels, f)


print "Features extracted and saved as features.pkl"