from PIL import Image, ImageEnhance
from pylab import *
from skimage import color, feature
from scipy.ndimage import filters
import sift, colorsys


def extract_hsv_features(imList):
    hfeatures = sfeatures = vfeatures = zeros([len(imList), 128])
    # assigning numerica label 0 -H , 1-S, V=2
    h_labels = slabels = vlabels = zeros([len(imList), ])
    for i, im in enumerate(imList):
        print "...processing ", im
        im = color.rgb2hsv(array(Image.open(im)))
        h, s, v = im[:,:,0], im[:,:,1], im[:,:,2]
        H_hist, h1 = histogram(h.flatten(), bins=128, density=True)
        hfeatures[i] = H_hist
        h_labels[i] = 0
        S_hist, h2 = histogram(s.flatten(), bins=128, density=True)
        sfeatures[i] = S_hist
        slabels[i] = 1
        V_hist, h3 = histogram(v.flatten(), bins=128, density=True)
        vfeatures[i] = V_hist
        vlabels[i] = 2
        print " finished"

    return hfeatures, sfeatures, vfeatures, h_labels, slabels, vlabels


def extract_gradient_features(imList):
    # assign label 4 for gradient features
    labels = zeros([len(imList), ])
    features = zeros([len(imList), 128])
    for i, im in enumerate(imList):
        im = array(Image.open(im).convert("L"))
        # print im.shape
        # Sobel derivative filters
        imx = zeros(im.shape)
        filters.sobel(im, 1, imx)

        imy = zeros(im.shape)
        filters.sobel(im, 0, imy)

        magnitude = sqrt(imx ** 2 + imy ** 2)
        hist, _ = histogram(magnitude.flatten(), bins=128, density=True)
        features[i] = hist
        labels[i] = 3


    return features, labels


def extract_sift_features(imList):
    # assign label 5 for each features
    descriptor = []

    features = zeros([len(imList), 128])
    labels = zeros([len(imList), ])
    for i, feat in enumerate(imList):
        sift.process_image(feat, feat[:-3]+'sift')
        loc1, descr = sift.read_features_from_file(feat[:-3]+'sift')
        # hist, _ = histogram(descr.flatten(), bins=128, density=True)
        # print descr.shape
        descriptor.append(descr.flatten())
        # features[i] = hist
        labels[i] = 4

    return array(descriptor), labels


def extract_contrast_features(imList):
    # assign label 6 for each features
    labels = zeros([len(imList), ])
    features = zeros([len(imList), 128])
    for i, img in enumerate(imList):
        im = Image.open(img)
        img = array(im)
        # obtained max and min intensity
        max = img.max() * 1.0
        min = img.min() * 1.0

        # compute the image contrast factor
        factor = (max - min) / (max + min)

        enhancer = ImageEnhance.Contrast(im)
        im2 = enhancer.enhance(factor)

        hist, _ = histogram(im2, bins=128,  density=True)
        features[i] = hist
        labels[i] = 5

    return features, labels


def extract_lbp_features(imList, numPoint=24, radius=8, eps=1e7):
    """ Extraction of local binary pattern with radius 8 x 8 boxes division"""
    features = zeros([len(imList), 128])
    labels = zeros([len(imList), ]) # assign 7
    for i, im in enumerate(imList):
        image = array(Image.open(im).convert("L"))
        lbp = feature.local_binary_pattern(image, numPoint, radius, method='uniform')
        hist, _ = histogram(lbp.flatten(),bins=128, density=True)
        features[i] =hist
        labels[i] = 6
        # # normalise the histogram
        # hist = hist.astype('float')
        # hist /= (hist.sum() + eps)

    return features, labels



