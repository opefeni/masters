import vocabulary
import features_extractor as ft
import os, pickle, sift
import knn
from libsvm.python.svmutil import *
from pylab import *

test_path = "dataset/test2"
training_path = "dataset/training2"


def generate_codeword(path,storage_name,voc_name):

    imlist =[os.path.join(path, f) for f in os.listdir(path) if f.endswith('.jpg')]

    labels = []

    # features = zeros([len(imlist), 896])
    features = []
    for i, im in enumerate(imlist):
        # extracting multiple features
        print " .....processing  ",im

        descr = ft.extract_sift_features(im)
        h, s, v = ft.extract_hsv_features(im)
        contrast = ft.extract_contrast_features(im)
        grad = ft.extract_gradient_features(im)
        lbp = ft.extract_lbp_features(im)
        descriptor = concatenate((descr, lbp, h, s, v, grad, contrast), axis=1)
        # features[i] = array(descriptor,'f')
        features.append(array(descriptor))
        labels.append(im.split('/')[-1][:2])


    # create a vocabulary
    #
    # voc = vocabulary.Vocabulary(voc_name)
    # voc.train(features)
    #
    # # saving vocabulary
    # with open(storage_name, 'wb') as f:
    #     pickle.dump(voc, f)
    #     pickle.dump(labels, f)
    #
    # print 'vocabulary is:', voc.name

    return features, labels


print "***********************************************"
print "Processing Training Dataset"
print "***********************************************"

training_features, training_labels = generate_codeword(training_path, 'weather_training.pkl', 'Training_Dataset')


print "***********************************************"
print "Processing Test Dataset"
print "***********************************************"

test_features, test_labels = generate_codeword(test_path, 'weather_test.pkl', 'Test_Dataset')


training_labels = array(training_labels)
test_labels = array(test_labels)

# print training_features.shape
# print test_features.shape


# using kNN classifier
k = 1
knn_classifier = knn.KnnClassifier(training_labels,training_features)
res = array([knn_classifier.classify(test_features[i],k) for i in range(len(test_labels))])

# compute knn accuracy
acc = sum(1.0*(res==test_labels)) / len(test_labels)
print 'KNN Accuracy:', acc


# using svm classifier
# create SVM
features = map(list, training_features)
test_features = map(list, test_features)

classname = unique(training_labels)


# create conversion function for the labels
def convert_labels(labels, transl):
    """ Convert between strings and numbers. """
    return [transl[l] for l in labels]


transl = {}

for i, c in enumerate(training_labels):
    transl[c], transl[i] = i, c

labels = convert_labels(training_labels, transl)

# print len(labels)
# print len(training_labels)

prob = svm_problem(labels, features)
param = svm_parameter('-t 2')

#train SVM on data
m = svm_train(prob, param)

res = svm_predict(convert_labels(test_labels, transl), test_features, m)[0]