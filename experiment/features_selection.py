import numpy as np
from sklearn.ensemble import RandomForestClassifier, ExtraTreesClassifier
from sklearn import datasets
from sklearn.model_selection import train_test_split
from sklearn.feature_selection import SelectFromModel
from sklearn.metrics import accuracy_score

# load dataset
iris = datasets.load_iris()

# create a list of feature names
feat_labels = ['Sepal Length', 'Sepal Width', 'Petal Length','Petal Width']

# create features and labels

X = iris.data
y = iris.target

# split data into 40% test and 60% training

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.4, random_state=0)

clf = RandomForestClassifier(n_estimators=1000, random_state=0, n_jobs=-1)

clf.fit(X_train, y_train)

# print name and the gini importance for each  features
for feature in zip(feat_labels, clf.feature_importances_):
        print  feature

# Create a selector object that will use the random forest classifier to identify
# features that have an importance of more than 0.15

sfm = SelectFromModel(clf, threshold=0.15)

sfm.fit(X_train,y_train)

for feature_list_index in sfm.get_support(indices=True):
    print feat_labels[feature_list_index]

# create a dataset with only the most important features
# note: we have to apply the transform to both the training X and test X data

X_important_train = sfm.transform(X_train)
X_important_test = sfm.transform(X_test)

# Train A new RandomFOrst Classifier Using only the most important features

clf_important = RandomForestClassifier (n_estimators=1000, random_state=0, n_jobs=-1)

# train the new classifier with the most important features
clf_important.fit(X_important_train, y_train)

# compare the accuracy of our ful features classifier to our limited feature classifier
# Apply the full featured classifier to test data
y_pred = clf.predict(X_test)

# View the accuracy of our full features(4 features) model;;
print accuracy_score(y_test, y_pred)

# apply our important features
y_important_pred = clf_important.predict(X_important_test)

# view accuracy of our limited features (2 features) model
print accuracy_score(y_test, y_important_pred)