******************************************
Author: Gbeminiyi Ajayi
******************************************
This is a work in progress research work. The alogrithm are written in python on virtualenv.
The steps carried out so far as follows:
This is research was built on the concept of bag of visual word. @
recognition benchmark data set was used and
The images features were extracted using SIFT and histogram were extracted and inserted in sqlite db.
Kmean clustering alogrithm and vector quantization was used to training the vocabulary and the
euclidean distance was used to find similarities between images.



*****************************************************
SETUP & RUN PROCEDURE
*****************************************************
1. Copy this url(https://bitbucket.org/opefeni/masters/downloads/) into your browser to download the zip
   or git clone https://bitbucket.org/opefeni/masters.git
3. open your terminal and change to your download directory
4. type cd path_to_your_download/unisa
5. type source bin/activate
6. type pip install -r requirements.txt
6. cd script
7. python run.py
