"""
.. codeauthor:: Ivano Lauriola <ivanolauriola@gmail.com>

================
Input validation
================

.. currentmodule:: MKLpy.utils.validation

This sub-package contains tool to check the input of a MKL algorithm.

"""
from scipy.sparse import issparse
from sklearn.utils import check_array
from sklearn.utils import column_or_1d, check_X_y
from cvxopt import matrix
import numpy as np
import types
 
 
def check_KL_Y(KL,Y):
    """check if the input is a valid kernel list compliant with MKLpy MKL algorithms.

    Parameters
    ----------
    KL : (l,n,m) array_like,
             where *l* is the number of kernels in list and *(n,m)* is the shape of kernels.
    Y : (n) array_like,
            where *n* is the number of samples
    
    Returns
    -------
    K_ : (l,n,m) ndarray or kernel_list.

    Notes
    -----
    The evaluation is not exaustive due to complexity of a regular list of kernels.
    """
    #TODO
    if not hasattr(KL,'__len__'):
        raise TypeError("list of kernels must be array-like")
    
    if type(KL) == list:
        KL = np.array(KL)
    elif KL.__class__.__name__ == 'kernel_list':
        KL = KL
    elif type(KL) != np.array:
        KL = np.array(list(KL))

    #return X

    if len(KL.shape) == 2:
        raise TypeError("Expected a list of kernels, found a matrix")
    if len(KL.shape) != 3:
        raise TypeError("Expected a list of kernels, found unknown")
    a = KL[0]
    check_X_y(a.T,Y, accept_sparse='csr', dtype=np.float64, order="C")
    
    if a.shape != (KL.shape[1],KL.shape[2]):
        raise TypeError("Incompatible dimensions")

    return KL,Y


def check_squared(K):
    if K.shape[0] != K.shape[1]:
        raise ValueError("K must be squared; shape obtained: "+str(K.shape))
    return K.todense() if issparse(K) else K


def check_K_Y(K,Y):
    K = check_squared(K)
    if len(Y) != K.shape[0]:
        raise ValueError("K and Y have different length")
    return K,np.array([1 if y == Y[0] else -1 for y in Y])

def check_X_T(X,T):
    T = X if type(T) == types.NoneType else T
    if X.shape[1] != T.shape[1]:
        raise ValueError("X and T have different number of features")
    return X,T



def check_cv(cv,Y):
    '''cv e' una lista di split'''
    #TODO
    pass


def check_scoring(estimator, scoring):
    #TODO
    return scoring

