from check_list import check_list
from check_arrange import check_arrange
from check_fastgen import check_fastgen
from check_regularization import check_regularization
from check_metrics import check_metrics


__all__ = ['check_list',
           'check_arrange',
           'check_fastgen',
           'check_regularization',
           'check_metrics']

def check_component(name, checker):
    print 'checking %s...' % name
    checker()
    #print 'done'

def check_MKLpy():
    check_component('list',           check_list)
    check_component('arrange',        check_arrange)
    check_component('fastgen',        check_fastgen)
    check_component('regularization', check_regularization)
    check_component('metrics',        check_metrics)
    print 'done'

#check_MKLpy()