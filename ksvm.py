"""@Author: Gbeminiyi Ajayi
@Timestamp: 08/06/2017 2:10AM
@description: SVM Clustering based on kmean 
"""

from scipy.cluster.vq import *
from pylab import *
from libsvm.python.svmutil import *

#generate a random class data
class1 = 1.5 * rand(10,2)
class2 = randn(10,2) + array([5,5])
class3 = randn(10,2) * 2.5 + array([2.5,7])

# combine the classes as a single feature
features = vstack((class1, class2, class3))


# perform kmean algorithm with vector quantization, this return the feature label
centroids, _ = kmeans(features, 3)
code,_ = vq(features, centroids)

# prepare data fit for svm classifier

label = list(code)
feat = map(list, features)

print "************************************"
print feat
print "************************************"


# divide feature and label into training and test
training_data = feat[::2]
training_label = label[::2]

test_data = feat[2::2]
test_label = label[2::2]

prob = svm_problem(training_label,training_data)
param = svm_parameter('-t 0')

m = svm_train(prob, param)
res = svm_predict(training_label,training_data, m)

res = svm_predict(test_label,test_data, m)[0]
