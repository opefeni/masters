from PIL import Image
from numpy import *

def pca(X):
    """ Principal Component Analysis
        Input: x, matrix of one dimension image stack for each image in a row
        return: projection matrix(with one important dimension first), variance, and mean
    """

    # get dimension
    m, n = X.shape

    # obtained the center data
    mean_X = X.mean(axis=0)
    X = X - mean_X

    if n > m :
        # Compact tricks
        M = dot(X, X.T) # covariance matrix
        e, EV = linalg.eigh(M) #eigenvalues and eigenvectors
        tmp = dot(X.T, EV).T # this is the trick
        V = tmp[::-1]
        S = sqrt(e)[::-1]
        for i in range(V.shape[1]):
            V[:, i] /= S

    else:
        #PCA - used SVD
        U, S, V = linalg.svd(X)
        V = V[:num_data]

    return V, S, mean_X


