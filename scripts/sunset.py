"""
@Author: Gbeminiyi Ajayi
@Timestamp: 08/06/2017
@description: SVM Clustering based on kmean 
"""

from scipy.cluster.vq import *
from pylab import *
from libsvm.python.svmutil import *
from PIL import Image
import os, pickle
import sift, hog, wfeatures

# #extract feature vector (8 bins per color channel)
# features = zeros([len(imlist), 512])
# for i, f in enumerate(imlist):
#     im = array(Image.open(f))
#     h, edges = histogramdd(im.reshape(-1, 3), 8, normed=True, range=[(0,255),(0,255),(0,255)])
#     features[i] = h.flatten()

# labels = [featfile.split('/')[-1][:2] for featfile in imlist]


def process_images(path):
    print "****************************************"
    print "Extracting image features"
    print "****************************************"

    imlist = [os.path.join(path, f) for f in os.listdir(path) if f.endswith('.jpg')]

    for im in imlist:
        sift.process_image(im, im[:-3]+'sift')
        hog.process_image_hog(im, im[:-3]+'hog', 90, 40, True)


def process_features(path):
    print "****************************************"
    print "Processing image features"
    print "****************************************"

    featureslist =[os.path.join(path, f) for f in os.listdir(path) if f.endswith('.jpg')]

    # initialised empty feature list
    # features = []
    labels = []
    #
    for feat in featureslist:
    #     # read each feature descriptor
    #     loc1, descr = sift.read_features_from_file(feat[:-3]+'sift')
    #     # loc2, d2 = sift.read_features_from_file(feat[:-3]+'hog'))
    #     features[i] = descr
        labels.append(feat.split('/')[-1][:2])

    # extract other features
    sfeat = wfeatures.extract_sift_features(featureslist)
    h, s, v = wfeatures.extract_hsv_features(featureslist)
    lbp = wfeatures.extract_lbp_features(featureslist)
    grad = wfeatures.extract_gradient_features(featureslist)
    contrast = wfeatures.extract_contrast_features(featureslist)

    print 'sift features dim ', sfeat.shape
    print 'contrast features dim ', contrast.shape
    print 'LBP features dim ', lbp.shape
    print 'Gradient features dim ', grad.shape
    im_features = vstack((sfeat,h,s,v,grad,lbp,contrast))

    with open('weather_rgb.pkl', 'wb') as f:
        pickle.dump(im_features, f)
        pickle.dump(labels, f)

    return im_features, labels


class svm_classifier(object):

    def __init__(self, training_data, training_label):
        self.training_data = training_data
        self.training_label = training_label
        self.model = ''

    def train(self):
        #setup libsvm parameters
        prob = svm_problem(convert_labels(self.training_label, transl), self.training_data)
        param = svm_parameter('-t 0')
        self.model = svm_train(prob, param)

        # training classifier
        res = svm_predict(self.convert_labels(training_label, transl), training_data, self.model)

    def classify(self, test_label, test_data):
        # test SVM
        res = svm_predict(self.convert_labels(test_label, transl), test_data, self.model)[0]
        # res = convert_labels(res, transl)
        # acc = sum(1.0 * (res == test_label)) / len(test_label)
        # print 'Accuracy:' , acc

    def convert_labels(self, labels, transl):
        """ Convert between strings and numbers. """
        return [transl[l] for l in labels]

    def print_confusion(self, res,labels,classnames):

        n = len(classnames)

        # confusion matrix
        class_ind = dict([(classnames[i],i) for i in range(n)])

        confuse = zeros((n,n))
        for i in range(len(test_label)):
            confuse[class_ind[res[i]],class_ind[test_label[i]]] += 1

        print 'Confusion matrix for'
        print classnames
        print confuse



test_path = "dataset/test"
training_path = "dataset/training"
print "[*] Training dataset....."
# process_images(training_path)
training_features, training_labels = process_features(training_path)

print "[*] Testing dataset....."
# process_images(test_path)
test_features, test_labels = process_features(test_path)

# with open('weather_rgb.pkl','rb') as f:
#     features = pickle.load(f)
#     labels = pickle.load(f)


def convert_labels(labels, transl):
    """ Convert between strings and numbers. """
    return [transl[l] for l in labels]


def print_confusion(res,labels,classnames):

    n = len(classnames)

    # confusion matrix
    class_ind = dict([(classnames[i],i) for i in range(n)])

    confuse = zeros((n,n))
    for i in range(len(labels)):
        confuse[class_ind[res[i]], class_ind[labels[i]]] += 1

    print 'Confusion matrix for'
    print classnames
    print confuse

# prepare data fit for svm classifier
training_labels = list(training_labels)
test_labels = list(test_labels)

training_features = map(list, training_features)
test_features = map(list, test_features)
classnames = unique(training_labels)

# perform kmean algorithm with vector quantization, this return the feature label

# print features
# centroids, variance = kmeans(features, 3)
# labels, distance = vq(features, centroids)

transl = {}

for i, c in enumerate(training_labels):
    transl[c], transl[i] = i, c



#create SVM
prob = svm_problem(convert_labels(training_labels, transl), training_features)
param = svm_parameter('-t 0')

#
m = svm_train(prob, param)
res = svm_predict(convert_labels(training_labels, transl), training_features, m)

# test SVM
res = svm_predict(convert_labels(test_labels, transl), test_features, m)[0]
res = convert_labels(res, transl)


# acc = sum(1.0 * (res == test_label)) / len(test_label)
# print 'Accuracy:' , acc

#
print_confusion(res, test_labels, classnames)
