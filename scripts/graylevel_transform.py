from PIL import Image
import numpy as np
import pylab as plt

def grayLevelTransform(im):
    #invert an image
    im2 = 255 - im

    #clamped image
    im3 = (100.0/255) * im + 100

    #squared
    im4 = 255.0 * (im/255)**2

    fig = plt.figure()
    plt.gray()
    ax1 =  fig.add_subplot(221)
    plt.title('Original Image')
    ax1.imshow(im)

    ax2 =  fig.add_subplot(222)
    plt.title('Inverted Image')
    ax2.imshow(im2)

    ax3 =  fig.add_subplot(223)
    plt.title('clamped Image')
    ax3.imshow(im3)

    ax4 =  fig.add_subplot(224)
    plt.title('Squared Image')
    ax4.imshow(im4)

    plt.show()
    
    
if __name__ == '__main__':    
    im = np.array(Image.open('castle.jpg').convert('L'))
    grayLevelTransform(im)


