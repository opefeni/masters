from PIL import Image
from harris import *

im1=im2=array(Image.open('vegetables.jpg').convert('L'))

#compute harris response
wid = 5
harrisim = compute_harris_response(im1,5)
filtered_coords1 = get_harris_point(harrisim, wid+1)
d1 =get_descriptors(im1, filtered_coords1, wid)

harrisim = compute_harris_response(im2,5)
filtered_coords2 = get_harris_point(harrisim, wid+1)
d2 =get_descriptors(im2, filtered_coords2, wid)

print "[+] Starting features detection and matching......."
matches = match_twosided(d1,d2)

figure()
gray()
plot_matches(im1,im2,filtered_coords1,filtered_coords2,matches[:100])
show()

