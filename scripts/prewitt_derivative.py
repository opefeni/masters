#!/bin/python

from PIL import Image
from scipy.ndimage import filters
from pylab import *

im = array(Image.open('castle.jpg').convert('L'))

"""prewitt derivative is given as:
    [-1 0 1]        [-1 -1 -1]
 x= [-1 0 1]    y=  [0  0   0]
    [-1 0 1]        [1   1  1]
"""

#compute x-derivative
imx = zeros(im.shape)
filters.prewitt(im,1,imx)

#compute y derivative
imy = zeros(im.shape)
filters.prewitt(im,0,imy)

magnitude = sqrt(imx**2 + imy**2)

print "[+] plotting..."
fig = figure()
gray()
ax1 = fig.add_subplot(221)
ax1.imshow(im)

ax2 = fig.add_subplot(222)
ax2.imshow(magnitude)

ax3 = fig.add_subplot(223)
ax3.imshow(imx)

ax4 = fig.add_subplot(224)
ax4.imshow(imy)


show()
