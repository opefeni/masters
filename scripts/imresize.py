from PIL import Image
import os,sys

def imageResize(path,prefix='IMG'):
    """Resize the image to 640 x 480 jpg and store them in a new directory"""

    print "[*] Fetching jpg image from "+path
    imList = [os.path.join(path,f) for f in os.listdir(path) if f.endswith('.jpg')]
    i = 0
    print "[+] Saving Images......."
    for image in imList:
        Image.open(image).resize((300,180)).save(image)
        print "..."+image +" saved resized "
        i+=1


if __name__ == '__main__':
    print '*******************************'
    print 'Starting Image Resizing Script'
    print '*******************************'
    imageResize(sys.argv[1])
