import vocabulary
import features_extractor as ft
import os, pickle, sift
import knn
from sklearn.model_selection import cross_val_score
from sklearn.naive_bayes import GaussianNB
from sklearn.svm import SVC
from sklearn.neighbors import KNeighborsClassifier
from sklearn.ensemble import VotingClassifier
from pylab import *

test_path = "dataset/test2"
training_path = "dataset/training2"


def generate_codeword(path,storage_name,voc_name):

    imlist =[os.path.join(path, f) for f in os.listdir(path) if f.endswith('.jpg')]

    labels = []

    # features = zeros([len(imlist), 896])
    features = []
    for i, im in enumerate(imlist):
        # extracting multiple features
        print " .....processing  ",im

        descr = ft.extract_sift_features(im)
        h, s, v = ft.extract_hsv_features(im)
        contrast = ft.extract_contrast_features(im)
        grad = ft.extract_gradient_features(im)
        lbp = ft.extract_lbp_features(im)
        descriptor = concatenate((descr, lbp, h, s, v, grad, contrast))
        # features[i] = array(descriptor,'f')
        features.append(array(descriptor))
        labels.append(im.split('/')[-1][:2])


    # create a vocabulary
    #
    # voc = vocabulary.Vocabulary(voc_name)
    # voc.train(features)
    #
    # # saving vocabulary
    # with open(storage_name, 'wb') as f:
    #     pickle.dump(voc, f)
    #     pickle.dump(labels, f)
    #
    # print 'vocabulary is:', voc.name

    return features, labels


print "***********************************************"
print "Processing Training Dataset"
print "***********************************************"

training_features, training_labels = generate_codeword(training_path, 'weather_training.pkl', 'Training_Dataset')


print "***********************************************"
print "Processing Test Dataset"
print "***********************************************"

test_features, test_labels = generate_codeword(test_path, 'weather_test.pkl', 'Test_Dataset')


# training_labels = array(training_labels
# test_labels = array(test_labels)

# create conversion function for the labels
def convert_labels(labels, transl):
    """ Convert between strings and numbers. """
    return [transl[l] for l in labels]


transl = {}

for i, c in enumerate(training_labels):
    transl[c], transl[i] = i, c

labels = convert_labels(training_labels, transl)


clf1 = KNeighborsClassifier(n_neighbors=10)
clf2 = SVC(kernel='rbf', probability='True')
clf3 = GaussianNB()

eclf = VotingClassifier(estimators=[('knn', clf1), ('svc', clf2), ('gnb', clf3)], voting='hard')


for clf, label in zip([clf1, clf2, clf3, eclf], ['KNN', 'SVM', 'Native Bayes','Ensemble']):
    score = cross_val_score(clf, X=training_features, y=labels, cv=5, scoring='accuracy')
    print "Accuracy: %0.2f (+/- %0.2f) [%s]" % (score.mean(), score.std(), label)
