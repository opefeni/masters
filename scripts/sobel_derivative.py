#!/bin/python

from PIL import Image
from scipy.ndimage import filters
from pylab import *

im = array(Image.open('vegetables.jpg').convert('L'))

#Sobel derivative

#compute x-derivative
imx = zeros(im.shape)
filters.sobel(im,1,imx)

#compute y derivative
imy = zeros(im.shape)
filters.sobel(im,0,imy)

magnitude = sqrt(imx**2 + imy**2)

print "[+] plotting..."
fig = figure()
gray()
ax1 = fig.add_subplot(221)
ax1.imshow(im)

ax2 = fig.add_subplot(222)
ax2.imshow(magnitude)

ax3 = fig.add_subplot(223)
ax3.imshow(imx)

ax4 = fig.add_subplot(224)
ax4.imshow(imy)


show()
