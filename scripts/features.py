import numpy as np
import matplotlib.pyplot as plt

from sklearn.datasets import load_iris
from sklearn.ensemble import ExtraTreesClassifier

dataset = load_iris()
X, y = dataset.d


#Build a classification task using 3 informative features

# X, y = make_classification(n_samples=1000, n_features=10,n_informative=3,n_redundant=0,n_repeated=0, n_classes=1, random_state=0,shuffle=False)
#
# forest = ExtraTreesClassifier(n_estimators=250, random_state=0)
#
# forest.fit(X,y)
#
# importance = forest.feature_importances_

# std = np.std([tree.feature_importances_ for tree in forest.n_estimators], axis=0)
#
# indices = np.argsort(importance)[::-1]
#
# #print the feature ranking
# for f in range(X.shape[1]):
#     print "(%d. feature %d (%f))" % (f+1, indices[f], importance[indices[f]])
#
# #plot the feature importance of the forest
# plt.figure()
# plt.title("Feature importances")
# plt.bar(range(X.shape[1]), importance[indices], color="r", yerr=std[indices], align="center")
# plt.xticks(range(X.shape[1]), indices)
# plt.xlim(-1, X.shape[-1])
# plt.show()
