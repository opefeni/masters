import pickle
from PIL import Image
import sqlite3 as sqlite
from pylab import *

class Indexer(object):

    def __init__(self, db, voc):
        """Initialized the database and vocabulary object"""

        self.conn = sqlite.connect(db)
        self.voc = voc

    def db_commit(self):
        self.conn.commit()

    def __del__(self):
        self.conn.close()

    def create_table(self):

        self.conn.execute("create table imlist(filename)")
        self.conn.execute("create table imwords(imid,wordid,vocname)")
        self.conn.execute("create table imhistogram(imid,histogram,vocname)")
        self.conn.execute("create index im_idx on imlist(filename)")
        self.conn.execute("create index wordid_idx on imwords(wordid)")
        self.conn.execute("create index imid_idx on imwords(imid)")
        self.conn.execute("create index imidhist_idx  on imhistogram(imid)")
        self.db_commit()

    def add_to_index(self, imname, descr):
        """Take an image with feature descriptor and project the vocabulary
            before adding to database
        """
        if self.is_indexed(imname): return
        print "Indexing ", imname

        # get image id
        imid = self.get_id(imname)

        # get the words
        imwords = self.voc.project(descr)
        num_words = imwords.shape[0]

        # link each word to image
        for i in range(num_words):
            word = imwords[i]
            # wordid is the word number itself
            self.conn.execute("insert into imwords(imid, wordid, vocname) VALUES (?,?,?)",(imid, word, self.voc.name))

        # store word histogram for image
        # use pickle to encode numpy as string
        self.conn.execute("insert into imhistogram(imid, histogram, vocname) VALUES (?,?,?)",(imid, pickle.dumps(imwords), self.voc.name))

    def is_indexed(self, imname):
        """Returned True if image is indexed"""

        im = self.conn.execute("select rowid from imlist where filename='%s'" % imname).fetchone()
        return im != None

    def get_id(self, imname):
        """ Get an rowid or insert if not present"""

        cur = self.conn.execute("select rowid from imlist where filename='%s'" % imname).fetchone()

        if cur == None:
            res = self.conn.execute("insert into imlist(filename) VALUES ('%s')" % imname)
            return res.lastrowid
        else:
            return cur[0]


class Searcher(object):
    def __init__(self, db, voc):
        """Initialized the database and vocabulary object"""

        self.conn = sqlite.connect(db)
        self.voc = voc

    def db_commit(self):
        self.conn.commit()

    def __del__(self):
        self.conn.close()

    def candidates_from_word(self, imwords):
        """ Get list of image words"""

        im_ids = self.conn.execute("select distinct imid from imwords where wordid=%d" % imwords).fetchall()
        return [i[0] for i in im_ids]

    def candidates_from_histogram(self, imword):
        """get list of images with similar word"""

        #get word is
        words = imword.nonzero()[0]

        # find candidate
        candidate =[]
        for word in words:
            c = self.candidates_from_word(word)
            candidate += c

        # take all unique words and reverse sort on occurrence
        tmp = [(w, candidate.count(w)) for w in set(candidate)]
        tmp.sort(cmp=lambda x, y: cmp(x[1], y[1]))
        tmp.reverse()

        # return sorted list, best matches first
        return [w[0] for w in tmp]

    def get_imhistogram(self, imname):
        """ Return the word histogram for an image. """

        im_id = self.conn.execute("select rowid from imlist where filename='%s'" % imname).fetchone()
        s = self.conn.execute("select histogram from imhistogram where rowid=%d" % im_id).fetchone()

        # use pickle to decode NumPy arrays from string
        return pickle.loads(str(s[0]))


    def query(self, imname):
        """ Find a list of matching images for imname"""

        h = self.get_imhistogram(imname)
        candidates = self.candidates_from_histogram(h)
        matchscores = []
        for imid in candidates:
            # get the name
            cand_name = self.conn.execute("select filename from imlist where rowid=%d" % imid).fetchone()
            cand_h = self.get_imhistogram(cand_name)
            cand_dist = sqrt(sum((h - cand_h) ** 2))  # use L2 distance
            matchscores.append((cand_dist, imid))


        # return a sorted list of distances and database ids
        matchscores.sort()
        return matchscores

    def get_filename(self, imid):
        """ Return the filename for an image id"""

        s = self.conn.execute("select filename from imlist where rowid='%d'" % imid).fetchone()
        return s[0]


def compute_score(src, imlist):
    """Returns the average number of correct images on the top of 4 result queries"""

    num_images = len(imlist)
    pos = zeros((num_images, 4))

    #get the first four result for each images
    for i in range(num_images):
        pos[i] = [w[1]-1 for w in src.query(imlist[i])[:4]]

    # compute score and return average
    score = array([(pos[i] // 4) == (i // 4) for i in range(num_images)]) * 1.0
    return sum(score) / num_images

def plot_results(src,res):
    """ Show images in result list res."""
    figure()
    nbr_results = len(res)
    for i in range(nbr_results):
        imname = src.get_filename(res[i])
        subplot(1,nbr_results,i+1)
        imshow(array(Image.open(imname)))
        axis('off')
    show()
