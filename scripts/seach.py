import pickle
import sift
import imtools
import imagesearch

imList = imtools.getImageList('./')
num_image = len(imList)
featlist = [imList[i][:-3]+'sift' for i in range(num_image)]



# load vocabulary
with open('vocabulary.pkl', 'rb') as f:
    voc = pickle.load(f)

# create indexer
indx = imagesearch.Indexer('demo.db', voc)

indx.create_table()

for i in range(num_image)[:100]:
    locs, descr = sift.read_features_from_file(featlist[i])
    indx.add_to_index(imList[i], descr)

# commit to the db
indx.db_commit()

src = imagesearch.Searcher('test.db', voc)
locs, descr = sift.read_features_from_file(featlist[6])
iw = voc.project(descr)
print 'ask using a histogram...'
print src.candidates_from_histogram(iw)[:10]