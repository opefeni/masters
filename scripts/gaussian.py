#!/bin/python

from PIL import Image
from scipy.ndimage import filters
from pylab import *


def gaussian_derivative(im, sigma=2):
    
    #Sobel derivative using  gaussian_filter
    #using standard deviation of 2

    #compute x-derivative
    imx = zeros(im.shape)
    filters.gaussian_filter(im,(sigma,sigma),(1,0), imx)

    #compute y derivative
    imy = zeros(im.shape)
    filters.gaussian_filter(im,(sigma,sigma),(0,1), imy)

    magnitude = sqrt(imx**2 + imy**2)

    return imx,imy,magnitude


if __name__ == '__main__':
    print "[+] plotting..."
    
    im = array(Image.open('banner.jpg').convert('L'))

    imx2,imy2,m2 = gaussian_derivative(im)
    imx5,imy5,m5 = gaussian_derivative(im,sigma=5)
    imx10,imy10,m10 = gaussian_derivative(im,sigma=10)

    fig = figure()
    gray()
    axis('off')

    #plot x derivatives
    ax2 = fig.add_subplot(331)
    ax2.imshow(imx2)

    ax3 = fig.add_subplot(332)
    ax3.imshow(imx5)

    ax4 = fig.add_subplot(333)
    ax4.imshow(imx10)

    #plot y derivatives

    ax2 = fig.add_subplot(334)
    ax2.imshow(imy2)

    ax3 = fig.add_subplot(335)
    ax3.imshow(imy5)

    ax4 = fig.add_subplot(336)
    ax4.imshow(imy10)

       #plot y derivatives
    ax2 = fig.add_subplot(337)
    ax2.imshow(m2)

    ax3 = fig.add_subplot(338)
    ax3.imshow(m5)

    ax4 = fig.add_subplot(339)
    ax4.imshow(m10)


    show()
