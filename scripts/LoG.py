#!/bin/python

from PIL import Image
from scipy.ndimage import filters
from pylab import *

def laplacian_of_gaussian(im,sigma=2):
    """ compute the laplacian_of_gaussian"""
    
    #compute  x-derivative
    imx = zeros(im.shape)
    filters.gaussian_filter(im,(sigma,sigma),(0,1), imx)

    #compute  y derivative
    imy = zeros(im.shape)
    filters.gaussian_filter(im,(sigma,sigma),(1,0), imy)

    LoG = sigma * sigma * (imx + imy)
    
    return LoG
    
    


if __name__ == '__main__':
    print "[+] Plotting ........"
    fig = figure()
    gray()
    
    im = array(Image.open('vegetables.jpg').convert('L'))
    
    im2 = laplacian_of_gaussian(im)
    im5 = laplacian_of_gaussian(im,5)
    im7 = laplacian_of_gaussian(im,7)
    im10 = laplacian_of_gaussian(im,10)

    axis('off')
    ax1 = fig.add_subplot(221)
    ax1.axis('off')
    title('Original Image')
    ax1.imshow(im)

    ax2 = fig.add_subplot(222)
    ax2.axis('off')
    title('Image scale = 2')
    ax2.imshow(im2)

    ax3 = fig.add_subplot(223)
    ax3.axis('off')
    title('Image scale=5')
    ax3.imshow(im5)

    ax4 = fig.add_subplot(224)
    title('Image scale=10')
    ax4.axis('off')
    ax4.imshow(im10)

    show()

