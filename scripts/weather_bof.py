import vocabulary
import wfeatures as ft
import os, pickle, sift
from pylab import *

test_path = "dataset/test"
training_path = "dataset/training"


def generate_codeword(path,storage_name,voc_name):

    imlist =[os.path.join(path, f) for f in os.listdir(path) if f.endswith('.jpg')]


    print "*************************************************"
    print "Extracting HSV features"
    print "**************************************************"
    h, s, v, hl, sl, vl = ft.extract_hsv_features(imlist)

    print "*************************************************"
    print "Extracting Gradient features"
    print "**************************************************"
    grad, g_label = ft.extract_gradient_features(imlist)


    print "*************************************************"
    print "Extracting sift features"
    print "**************************************************"
    sf, sf_label = ft.extract_sift_features(imlist)

    print "*************************************************"
    print "Extracting contrast features"
    print "**************************************************"
    contrast, con_label= ft.extract_contrast_features(imlist)


    print "*************************************************"
    print "Extracting LBP features"
    print "**************************************************"
    lbp, lbp_label = ft.extract_lbp_features(imlist)

    # same other with the label
    features = concatenate((h, s, v, grad, sf, contrast, lbp))
    labels = concatenate((hl, sl, vl, g_label, sf_label, con_label, lbp_label))



    # create a vocabulary

    # voc = vocabulary.Vocabulary(voc_name)
    # voc.train(array(features))

    # saving vocabulary
    with open(storage_name, 'wb') as f:
        pickle.dump(features, f)
        pickle.dump(labels, f)

    print 'Files stored as :', storage_name


print "***********************************************"
print "Processing Training Dataset"
print "***********************************************"

generate_codeword(training_path,'weather_training.pkl','Training_Dataset')


print "***********************************************"
print "Processing Test Dataset"
print "***********************************************"

generate_codeword(test_path,'weather_test.pkl','Test_Dataset')

