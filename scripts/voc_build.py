import pickle
import vocabulary2
import sift
from pylab import *

# initialized the sift constructor to obtain the image list and feature list using SIFT
imList, featList = sift.constructor('./')


# get the number of images
num_image = len(imList)

voc = vocabulary2.Vocabulary('demo')
voc.train(featList, 500, 10)

#saving the object using pickle
with open('vocabulary.pkl','wb') as f:
    pickle.dump(voc, f)

print "Vocabulary is: ", voc.name, voc.num_words