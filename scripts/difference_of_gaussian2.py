import numpy as np
import pylab as plt
from mpl_toolkits.mplot3d import Axes3D

def gaussian_2d(x, y, x0, y0, xsig, ysig):
    return np.exp(-0.5*(((x-x0) / xsig)**2 + ((y-y0) / ysig)**2))

delta = 0.1
x = np.arange(-6.0, 6.0, delta)
y = np.arange(-6.0, 6.0, delta)
X, Y = np.meshgrid(x, y)
Z1 = gaussian_2d(X, Y, 0., 0., 1., 1.)
Z2 = gaussian_2d(X, Y, 1., 1., 1.5, 1.5)
# difference of Gaussians
Z = 10.0 * (Z2 - Z1)


fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
CS = ax.plot_surface(X, Y, Z)
plt.clabel(CS, inline=1, fontsize=10)
plt.title('Guassian Graph')
plt.show()
