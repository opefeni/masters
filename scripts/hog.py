import sift
from PIL import Image
from numpy import *
import os
import imtools

def process_image_hog(imname,outname,size=20, steps=10, force_orientation=False, resize=None):
    """ Process an image with densely sampled SIFT descriptors and save the results in a file.
       Optional input: size of features, steps between locations, forcing computation of
       descriptor orientation (False means all are oriented upwards), tuple for resizing the image."""

    im = Image.open(imname).convert('L')

    if resize != None:
        im.resize(size)

    m, n = im.size

    if imname[-3] != 'pgm':
        #create new image
        im.save('tmp.pgm')
        imname = 'tmp.pgm'

    #create frames and save to temporary file
    scale = size/3.0

    x, y = meshgrid(range(steps, m, steps), range(steps, n, steps))
    xx, yy = x.flatten(), y.flatten()
    frame = array([xx, yy, scale*ones(xx.shape[0]), zeros(xx.shape[0])])
    savetxt('tmp.frame',frame.T, fmt='%03.3f')

    if force_orientation:
        cmmd = str("bin/glnxa64/sift "+imname+" --output="+outname+" --read-frames=tmp.frame --orientations")
    else:
        cmmd = str("bin/glnxa64/sift "+imname+" --output="+outname+" --read-frames=tmp.frame")

    os.system(cmmd)

    print "processed ", imname, " to ", outname