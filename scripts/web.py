from flask import Flask, render_template
import imtools, imagesearch, pickle, os
from numpy import *
app = Flask('__name__')


@app.route('/')
def index():
    display = 16
    imlist = imtools.getImageList('static/ukbench')

    num_image = len(imlist)
    im_index = range(num_image)

    # load vocabulary
    with open('vocabulary.pkl', 'rb') as f:
        voc = pickle.load(f)

    src = imagesearch.Searcher('ukbench.db', voc)
    imname = []

    random.shuffle(imlist)
    for i in im_index[:display]:
        imname.append(imlist[i])

    return render_template('index.html', imname=imname)

@app.route('/search/<query>')
def search(query):
    display = 16
    imlist = imtools.getImageList('static/ukbench')

    num_image = len(imlist)
    im_index = range(num_image)

    # load vocabulary
    with open('vocabulary.pkl', 'rb') as f:
        voc = pickle.load(f)

    src = imagesearch.Searcher('ukbench.db', voc)
    imname = []
    if query:
        query = './ukbench/' + query
        res = src.query(query)[:display]
        for dist, imid in res:
            imname.append(src.get_filename(imid))
    else:
        random.shuffle(imlist)
        for i in im_index[:display]:
            imname.append(imlist[i])

    return render_template('search.html', imname=imname)




if __name__ == '__main__':
    app.run(debug=True)