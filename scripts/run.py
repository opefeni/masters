import sift
import imagesearch
import pickle
import imtools


imList = imtools.getImageList('./ukbench')
num_image = len(imList)
featlist = [imList[i][:-3]+'sift' for i in range(num_image)]

# load vocabulary
with open('vocabulary.pkl', 'rb') as f:
    voc = pickle.load(f)

src = imagesearch.Searcher('ukbench.db',voc)
locs,descr = sift.read_features_from_file(featlist[70])
iw = voc.project(descr)
print 'obtaining feature histogram match...'
print src.candidates_from_histogram(iw)[:10]
print 'query based on euclidean distance to compute the closest match...'
print src.query(imList[70])[:10]
print 'Accuracy ', imagesearch.compute_score(src, imList[:19])
nbr_results = 6
res = [w[1] for w in src.query(imList[70])[:nbr_results]]
imagesearch.plot_results(src, res)


