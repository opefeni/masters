from PIL import Image
from imtools import *

im = array(Image.open('vegetables.jpg').convert('L'))
im2,cdf = histeq(im)


fig = figure()

axis('off')

fig.add_subplot(231)
hist(im.flatten(),128)

fig.add_subplot(232)
plot(cdf)

ax2=fig.add_subplot(233)
hist(im2.flatten(),128)

gray()
ax1=fig.add_subplot(234)
ax1.imshow(im)

ax2=fig.add_subplot(236)
ax2.imshow(im2)

show()
