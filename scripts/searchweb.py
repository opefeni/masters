import pickle, os
import imagesearch, imtools
from pylab import *

class Searchweb(object):

    def __init__(self):
        self.display = 15
        self.imlist = imtools.getImageList('./ukbench')

        num_image = len(self.imlist)
        self.im_index = range(num_image)
        featlist = [imlist[i][:-3]+'sift' for i in range(num_image)]

        # load vocabulary
        with open('vocabulary.pkl', 'rb') as f:
            self.voc = pickle.load(f)

    def imsearch(self,query=None):
        self.src = imagesearch.Searcher('ukbench.db', self.voc )

        if query:
            res = self.src.query(query)[:self.display]

