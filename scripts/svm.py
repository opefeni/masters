import pickle
from libsvm.python.svmutil import *

# load 2D points
with open('points_normal.pkl','r') as f:
    class_1 = pickle.load(f)
    class_2 = pickle.load(f)
    labels = pickle.load(f)

# convert to lists for libsvm
class_1 = map(list, class_1)
class_2 = map(list, class_2)
labels = list(labels)
samples = class_1 + class_2

print labels

# create SVM 
prob = svm_problem(labels, samples)
param = svm_parameter('-t 2')

#train SVM on data
m = svm_train(prob, param)

res = svm_predict(labels, samples, m)

# print res
