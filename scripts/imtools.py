from PIL import Image
from pylab import *
import os

def getImageList(path):
    """return all array list of images in a directory"""
    
    return [os.path.join(path,f) for f in os.listdir(path) if f.endswith('.jpg')]


def getSiftList(path):
    """return all array list of images in a directory"""

    return [os.path.join(path, f) for f in os.listdir(path) if f.endswith('.sift')]

def histeq(im, nbr_bins=256):
    """ Historgram equalization of a grayscale image"""

    #get image histrogram
    imhist,bins = histogram(im.flatten(), nbr_bins,normed=True)
    cdf = imhist.cumsum() # cumulative distribution function
    cdf = 255 * cdf / cdf[-1] # normalize
    
    #use linear interpolation of cdf to find new pixel values
    im2 = interp(im.flatten(), bins[:-1], cdf)

    return im2.reshape(im.shape), cdf

def compute_average(imlist):
    """ Compute the average of a list of images. """
    #open first image and make into array of type float
    averageim = array(Image.open(imlist[0]), 'f')
    
    for imname in inlist[1:]:
        try:
            averageim += array(Image.open(imname))
        except:
            print imname + '....skipped'
    
    averageim/=len(imlist)
    
    return array(averageim, 'uint8')
