import pickle
import vocabulary
import sift
import imtools
import imagesearch
from pylab import *

# initialized the sift constructor to obtain the image list and feature list using SIFT
imList, featList = sift.constructor('./ukbench')

# imList = imtools.getImageList('./ukbench')
#
# # get the number of images
# num_image = len(imList)
# featList = [imList[i][:-3]+'sift' for i in range(num_image)]

print "[*] Building and Training of Vocabulary "
voc = vocabulary.Vocabulary('ukbench')
voc.train(featList, 1000, 10)

#saving the object using pickle
with open('vocabulary.pkl','wb') as f:
    pickle.dump(voc, f)

print "Vocabulary is: ", voc.name, voc.num_words


# load vocabulary
with open('vocabulary.pkl', 'rb') as f:
    voc = pickle.load(f)

print "[*] Creating database and data tables"
# create indexer
indx = imagesearch.Indexer('ukbench.db', voc)

indx.create_table()

print "[*] Start Image Indexing...... "
for i in range(num_image):
    locs, descr = sift.read_features_from_file(featList[i])
    indx.add_to_index(imList[i], descr)

# commit to the db
indx.db_commit()