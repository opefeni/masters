import pickle
import knn
from libsvm.python.svmutil import *
from pylab import *

# open training features and label
with open('weather_training.pkl','rb') as f:
    training_voc = pickle.load(f)
    training_labels = pickle.load(f)

# open test features and label
with open('weather_test.pkl','rb') as f:
    test_voc = pickle.load(f)
    test_labels = pickle.load(f)


training_features = training_voc.feature_vectors
test_features = test_voc.feature_vectors

training_labels = array(training_labels)
test_labels = array(test_labels)

print training_features.shape
print test_features.shape


# # using kNN classifier
# k = 1
# knn_classifier = knn.KnnClassifier(training_labels[:90],training_features[:90])
# res = array([knn_classifier.classify(test_features[i],k) for i in range(len(test_labels))])

# # compute knn accuracy
# acc = sum(1.0*(res==test_labels)) / len(test_labels)
# print 'KNN Accuracy:', acc


# using svm classifier
# create SVM 
features = map(list, training_features)
test_features = map(list, test_features)

classname = unique(training_labels)

# create conversion function for the labels
def convert_labels(labels, transl):
    """ Convert between strings and numbers. """
    return [transl[l] for l in labels]


transl = {}

for i, c in enumerate(training_labels):
    transl[c], transl[i] = i, c


prob = svm_problem(convert_labels(training_labels, transl), features)
param = svm_parameter('-t 2')

#train SVM on data
m = svm_train(prob, param)

res = svm_predict(convert_labels(training_labels, transl), test_features , m)[0]




